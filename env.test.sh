export DELI_HTTP_PORT=8085
export DELI_DB_PORT=5432
export DELI_DB_HOST="localhost"
export DELI_DB_DATABASE="deli"
export DELI_DB_USER="postgres"
export DELI_DB_PASSWORD="postgres"
export DELI_DB_CONNSTRING="host=${DELI_DB_HOST} \
  dbname=${DELI_DB_DATABASE} \
  user=${DELI_DB_USER} \
  password=${DELI_DB_PASSWORD} \
  sslmode=allow"
