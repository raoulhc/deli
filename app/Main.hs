module Main (main) where

import Database.PostgreSQL.Simple (connectPostgreSQL)
import Log
import Log.Backend.StandardOutput (withStdOutLogger)
import Network.Wai.Handler.Warp (run)

import Deli.Server (app)
import Servant.Auth.Server

main :: IO ()
main = do
  withStdOutLogger $ \logger -> do
    -- Make this configurable
    connection <- connectPostgreSQL
      "host='localhost' port=5432 user='postgres' password='postgres' dbname='deli'"
    runLogT "Deli" logger LogInfo $ logInfo_ "Got postgresql connection"
    jwk <- generateKey
    run 8085 (app jwk connection logger)
