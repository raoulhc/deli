# Largely some utilities to help getting up and running and testing

db-create:
	@createdb -h $(DELI_DB_HOST) -p $(DELI_DB_PORT) -U $(DELI_DB_USER) $(DELI_DB_DATABASE)

db-init:
	@migrate init "$(DELI_DB_CONNSTRING)"

db-migrate:
	@migrate migrate "$(DELI_DB_CONNSTRING)" migrations/

db-setup: db-create db-init db-migrate
