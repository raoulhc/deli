{-# LANGUAGE UndecidableInstances #-}

module Effectful.Opaleye (DB, runOpaleye, MonadDB (..)) where

import Data.Kind

import Data.Profunctor.Product.Default
import Database.PostgreSQL.Simple
import Effectful
import Effectful.Dispatch.Static
import Opaleye

type DB :: (Type -> Type) -> Type -> Type
data DB :: Effect

type instance DispatchOf DB = 'Static 'WithSideEffects
newtype instance StaticRep DB = DB Connection

runOpaleye :: IOE :> es => Connection -> Eff (DB ': es) a -> Eff es a
runOpaleye = evalStaticRep . DB

-- Note this almost certain will need some refining.
type MonadDB :: (Type -> Type) -> Constraint
class Monad m => MonadDB m where
  select :: Default FromFields a b => Select a -> m [b]
  insert :: Insert a -> m a

instance (DB :> es) => MonadDB (Eff es) where
  select q = do
    DB connection <- getStaticRep
    unsafeEff_ $ runSelect connection q
  insert q = do
    DB connection <- getStaticRep
    unsafeEff_ $ runInsert connection q
