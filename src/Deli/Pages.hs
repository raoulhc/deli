{-# OPTIONS_GHC -Wno-orphans #-}

module Deli.Pages where

import Deli.Prelude hiding ((:>))

import Lucid.Htmx

import Deli.Routes
import Lucid.Servant
import Servant

instance ToHtml a => ToHtml (DeliResponse a) where
  toHtml DeliResponse{..} = do
    doctype_
    title_ "Deli"
    header_ "Deli"
    script_ [src_ "https://unpkg.com/htmx.org@1.9.2"] ()
    script_ [src_ "https://unpkg.com/hyperscript.org@0.9.9"] ()
    script_ [src_ "app.js"] ()
    link_ [rel_ "stylesheet", linkAbsHref_ $ fieldLink style]
    body_ $ do
      div_ [class_ "topnav"] $ do
        a_ [href_ "/links"] "Links"
        a_ [href_ "/tags"] "Tags"
        a_ [href_ "/users"] "Users"
        div_ [class_ "topnav-right"] $ do
          maybe
            ( do
                a_ [href_ "/register"] "Register"
                a_ [href_ "/login"] "Login"
            )
            ( \user -> do
                a_ (toHtml $ display user)
                a_ [hxPost_ "/api/session/logout"] "Logout"
            )
            mUser
      div_ [class_ "content-box"] $ toHtml deliData
  toHtmlRaw DeliResponse{..} = toHtmlRaw deliData

instance ToJSON a => ToJSON (DeliResponse a) where
  toJSON DeliResponse{..} = toJSON deliData

constructDeliResponse :: Maybe User -> a -> Eff es (DeliResponse a)
constructDeliResponse muser = do
  pure . DeliResponse muser
