module Deli.Routes.Links where

import Deli.Prelude hiding ((:>))

import Data.Text qualified as T
import Data.Vector qualified as V

import Servant
import Web.FormUrlEncoded

import Deli.Routes.Utils

type LinkResponse :: Type
data LinkResponse = LinkResponse
  { link_uri :: Text
  , user_name :: Text
  , tags :: Vector Text
  }
  deriving stock (Generic)

instance ToJSON LinkResponse
instance FromJSON LinkResponse
instance FromForm LinkResponse where
  fromForm f =
    LinkResponse
      <$> parseUnique "link_uri" f
      <*> (fromMaybe "raoul" <$> parseMaybe "user_name" f)
      <*> (V.fromList . T.splitOn "," <$> parseUnique "tags" f)

type LinkPage :: Type
newtype LinkPage = LinkPage Bool
  deriving stock (Generic)

type LinkApiRoutes :: Type -> Type
data LinkApiRoutes mode = LinkApiRoutes
  { getLinks :: mode :- Get '[HTML, JSON] [LinkResponse]
  , getUserLinks
      :: mode
        :- Capture "user" Text
        :> Get '[HTML, JSON] [LinkResponse]
  , postLink
      :: mode
        :- AuthProtect "cookie-auth"
        :> ReqBody '[FormUrlEncoded, JSON] LinkResponse
        :> PostRedirect '[Header "HX-Redirect" Text]
  }
  deriving stock (Generic)
