module Deli.Routes.Tags where

import Deli.Prelude hiding ((:>))
import Deli.Routes.Links
import Servant

type TagPage :: Type
data TagPage = TagPage

type TagLinksResponse :: Type
data TagLinksResponse = TagLinksResponse
  { tag :: Text
  , links :: [LinkResponse]
  }
  deriving stock (Generic)

type TagsResponse :: Type
newtype TagsResponse = TagsResponse
  { tags :: [Text]
  }
  deriving stock (Generic)

type TagApiRoutes :: Type -> Type
data TagApiRoutes mode = TagApiRoutes
  { getTag
      :: mode
        :- Capture "tag" Text
        :> Get '[JSON, HTML] TagLinksResponse
  , getTags
      :: mode
        :- Get '[JSON, HTML] TagsResponse
  }
  deriving stock (Generic)
