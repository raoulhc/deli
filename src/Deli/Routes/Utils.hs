module Deli.Routes.Utils where

import Deli.Prelude

import Servant

type PostRedirect :: [Type] -> Type
type PostRedirect hs =
  Verb
    'POST
    303
    '[PlainText]
    (Headers hs NoContent)
