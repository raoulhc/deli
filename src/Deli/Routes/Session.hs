module Deli.Routes.Session where

import Deli.Prelude hiding ((:>))

import Data.Text.Lazy.Builder (fromText)
import Servant
import Servant.Auth.Server
import Web.FormUrlEncoded

type LoginPage :: Type
data LoginPage = LoginPage

type RegPage :: Type
data RegPage = RegPage

type LoginForm :: Type
data LoginForm = LoginForm
  { username :: Text
  , password :: Text
  }
  deriving stock (Generic)

instance FromJSON LoginForm
instance FromForm LoginForm

type LoginReject :: Type
data LoginReject = LoginReject
  deriving stock (Generic)

instance ToJSON LoginReject

type RegistrationAccepted :: Type
data RegistrationAccepted = RegistrationAccepted
  deriving stock (Generic)

instance ToJSON RegistrationAccepted

instance ToHtml RegistrationAccepted where
  toHtmlRaw = toHtml
  toHtml RegistrationAccepted = "Registered!"

-- TODO Add details to this
type RegistrationRejected :: Type
newtype RegistrationRejected = RegistrationRejected Text
  deriving stock (Generic)
  deriving newtype (ToHtml)

instance ToJSON RegistrationRejected

type RegisterForm :: Type
data RegisterForm = RegisterForm
  { username :: Text
  , password :: Text
  , email :: Text
  }
  deriving stock (Generic)

instance FromJSON RegisterForm
instance FromForm RegisterForm

instance Display RegisterForm where
  displayBuilder RegisterForm{..} =
    "RegisterForm { username = "
      <> fromText username
      <> ", password = [REDACTED]"
      <> ", email = "
      <> fromText email
      <> " }"

-- This gives us the headers to set or clear login cookies and redirect
type LoginHeaders :: [Type]
type LoginHeaders =
  '[ Header "Set-Cookie" SetCookie
   , Header "Set-Cookie" SetCookie
   , Header "HX-Redirect" Text
   ]

type LogoutHeaders :: [Type]
type LogoutHeaders =
  '[ Header "Set-Cookie" SetCookie
   , Header "Set-Cookie" SetCookie
   , Header "HX-Redirect" Text
   ]

type LoginResponses :: [Type]
type LoginResponses =
  '[ WithStatus 401 LoginReject
   , WithStatus
      303
      (Headers LoginHeaders NoContent)
   ]

type SessionApiRoutes :: Type -> Type
data SessionApiRoutes mode = SessionApiRoutes
  { sessionLogin
      :: mode
        :- "login"
        :> ReqBody '[FormUrlEncoded, JSON] LoginForm
        :> UVerb 'POST '[HTML, JSON] LoginResponses
  , sessionLogout
      :: mode
        :- "logout"
        :> Verb
            'POST
            303
            '[HTML, JSON]
            (Headers LogoutHeaders NoContent)
  , sessionRegister
      :: mode
        :- "register"
        :> ReqBody '[FormUrlEncoded, JSON] RegisterForm
        :> UVerb
            'POST
            '[HTML, JSON]
            '[ WithStatus 200 RegistrationAccepted
             , WithStatus 400 RegistrationRejected
             ]
  }
  deriving stock (Generic)
