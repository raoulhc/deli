module Deli.Routes.Users where

import Deli.Model.Users qualified as M
import Deli.Prelude hiding ((:>))
import Servant

type UserPage :: Type
data UserPage = UserPage
  deriving stock (Generic)

type UserApiRoutes :: Type -> Type
newtype UserApiRoutes mode = UserApiRoutes
  { getUsers
      :: mode
        :- QueryParam "user" Text
        :> Get '[HTML, JSON] [M.User]
  }
  deriving stock (Generic)
