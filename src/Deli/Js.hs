{-# LANGUAGE TemplateHaskell #-}

module Deli.Js where

import Data.ByteString
import Data.FileEmbed
import Network.HTTP.Types
import Network.Wai
import Servant

appjsHandler :: ServerT Raw m
appjsHandler = Tagged $ \_ resp ->
  resp . responseLBS status200 [] $ fromStrict $(embedFile "js/app.js")
