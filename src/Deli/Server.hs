module Deli.Server (app) where

import Deli.Prelude

import Data.String.Conversions (cs)
import Database.PostgreSQL.Simple (Connection)

import Network.Wai
import Servant hiding ((:>))
import Servant.Server.Generic

import Crypto.JOSE.JWK (JWK)

import Deli.Pages

-- Get all the instances of ToHTML and ToJSON

import Deli.Js
import Deli.Pages.Links ()
import Deli.Pages.Session ()
import Deli.Pages.Tags ()
import Deli.Pages.Users ()
import Deli.Routes
import Deli.Routes.Links
import Deli.Routes.Session
import Deli.Routes.Tags
import Deli.Routes.Users
import Deli.Style

import Deli.Server.Auth
import Deli.Server.Links

import Deli.Server.Session
import Deli.Server.Tags
import Deli.Server.Users
import Deli.Server.Utils

-- Set up out custom 404
notFoundFormatter :: NotFoundErrorFormatter
notFoundFormatter req =
  err404
    { errHeaders = [("Content-Type", "text/html")]
    , errBody = cs $ "Couldn't find: " <> rawPathInfo req
    }

customFormatters :: ErrorFormatters
customFormatters =
  defaultErrorFormatters
    { notFoundErrorFormatter = notFoundFormatter
    }

pageHandler :: Maybe User -> PageRoutes (AsServerT Deli)
pageHandler mUser =
  PageRoutes
    { linksPage = constructDeliResponse mUser (LinkPage $ isJust mUser)
    , usersPage = constructDeliResponse mUser UserPage
    , tagsPage = constructDeliResponse mUser TagPage
    , loginPage = constructDeliResponse mUser LoginPage
    , registerPage = constructDeliResponse mUser RegPage
    }

apiHandler :: ApiRoutes (AsServerT Deli)
apiHandler =
  ApiRoutes
    { linksApiRoutes = linksApiHandler
    , usersApiRoutes = usersApiHandler
    , tagsApiRoutes = tagsApiHandler
    , sessionApiRoutes = sessionApiHandler
    }

records :: Routes (AsServerT Deli)
records =
  Routes
    { home = flip constructDeliResponse ()
    , pageRoutes = pageHandler
    , apiRoutes = apiHandler
    , style = styleHandler
    , appjs = appjsHandler
    }

app :: JWK -> Connection -> Logger -> Application
app jwk connection logger =
  genericServeTWithContext
    trans
    records
    ( optAuthHandler jwk connection logger
        :. authHandler jwk connection logger
        :. customFormatters
        :. EmptyContext
    )
 where
  trans :: forall a. Deli a -> Handler a
  trans = effToHandler jwk connection logger
