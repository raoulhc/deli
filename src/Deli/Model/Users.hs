{-# LANGUAGE TemplateHaskell #-}

module Deli.Model.Users where

import Control.Monad (void)
import Data.Kind (Type)
import Data.Maybe (listToMaybe)
import Data.Text (Text)
import Data.Text.Display
import Data.Text.Lazy.Builder (fromText)
import GHC.Generics (Generic)

import Data.Profunctor.Product.TH (makeAdaptorAndInstance)
import Opaleye

import Data.Time
import Data.UUID (UUID)

import Effectful
import Effectful.Opaleye

-- We encode hash and salt as base64 encoded text types so be careful when
-- interacting with these values. Opaleye doesn't seem to retrieve bytea values
-- the same as they get put in which this approach avoids.
-- Another db might help with this, though the model should not leek this.

-- Boilerplate required to get a user
type UserPoly :: Type -> Type -> Type -> Type -> Type -> Type -> Type -> Type
data UserPoly user_id username email password_hash password_salt created_at updated_at = User
  { user_id :: user_id
  , username :: username
  , email :: email
  , password_hash :: password_hash
  , password_salt :: password_salt
  , created_at :: created_at
  , updated_at :: updated_at
  }

type UserWrite :: Type
type UserWrite =
  UserPoly
    (Maybe (Field SqlUuid))
    (Field SqlText)
    (Field SqlText)
    (Field SqlText)
    (Field SqlText)
    (Maybe (Field SqlTimestamptz))
    (Maybe (Field SqlTimestamptz))

type UserRead :: Type
type UserRead =
  UserPoly
    (Field SqlUuid)
    (Field SqlText)
    (Field SqlText)
    (Field SqlText)
    (Field SqlText)
    (Field SqlTimestamptz)
    (Field SqlTimestamptz)

type User :: Type
type User = UserPoly UUID Text Text Text Text UTCTime UTCTime

$(makeAdaptorAndInstance "pUser" ''UserPoly)

deriving stock instance Generic User
instance Display User where
  displayBuilder u = fromText u.username

userTable :: Table UserWrite UserRead
userTable =
  table "users" $
    pUser
      User
        { user_id = tableField "user_id"
        , username = tableField "username"
        , email = tableField "email"
        , password_hash = tableField "password_hash"
        , password_salt = tableField "password_salt"
        , created_at = tableField "created_at"
        , updated_at = tableField "updated_at"
        }

getUsers :: DB :> es => Eff es [User]
getUsers = select $ selectTable userTable

getUser :: DB :> es => Text -> Eff es (Maybe User)
getUser user = do
  fmap listToMaybe . select $ do
    u@User{username} <- selectTable userTable
    where_ $ sqlStrictText user .== username
    pure u

constructUser :: Text -> Text -> Text -> Text -> UserWrite
constructUser name email pw_hash pw_salt =
  User
    Nothing
    (sqlStrictText name)
    (sqlStrictText email)
    (sqlStrictText pw_hash)
    (sqlStrictText pw_salt)
    Nothing
    Nothing

putUsers :: DB :> es => [UserWrite] -> Eff es ()
putUsers users =
  void . insert $ Insert userTable users rCount Nothing
