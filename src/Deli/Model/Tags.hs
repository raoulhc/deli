{-# LANGUAGE TemplateHaskell #-}

module Deli.Model.Tags where

import Deli.Prelude

import Data.Aeson (Value (..))

import Data.Profunctor.Product.TH (makeAdaptorAndInstance)
import Opaleye

-- Tag Boilerplate

type TagPoly :: Type -> Type -> Type
data TagPoly tag_id tag = Tag
  { tag_id :: tag_id
  , tag :: tag
  }

type TagWrite :: Type
type TagWrite = TagPoly (Maybe (Field SqlUuid)) (Field SqlText)

type TagRead :: Type
type TagRead = TagPoly (Field SqlUuid) (Field SqlText)

type Tag :: Type
type Tag = TagPoly UUID Text

$(makeAdaptorAndInstance "pTag" ''TagPoly)

deriving stock instance Generic Tag

tagTable :: Table TagWrite TagRead
tagTable =
  table "tags" $
    pTag
      Tag
        { tag_id = tableField "tag_id"
        , tag = tableField "tag"
        }

instance ToJSON Tag where
  toJSON (Tag _ tag) = String tag
