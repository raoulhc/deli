{-# LANGUAGE TemplateHaskell #-}

module Deli.Model.Links where

import Deli.Prelude

import Data.Profunctor.Product.TH (makeAdaptorAndInstance)
import Data.Time (UTCTime)
import Opaleye

-- Link boilerplate

type LinkPoly :: Type -> Type -> Type -> Type -> Type
data LinkPoly link_id link_uri user_id created_at = Link
  { link_id :: link_id
  , link_uri :: link_uri
  , user_id :: user_id
  , created_at :: created_at
  }

type LinkRead :: Type
type LinkRead =
  LinkPoly
    (Field SqlUuid)
    (Field SqlText)
    (Field SqlUuid)
    (Field SqlTimestamptz)

type LinkWrite :: Type
type LinkWrite =
  LinkPoly
    (Maybe (Field SqlUuid))
    (Field SqlText)
    (Field SqlUuid)
    (Maybe (Field SqlTimestamptz))

type Link :: Type
type Link = LinkPoly UUID Text UUID UTCTime

$(makeAdaptorAndInstance "pLink" ''LinkPoly)

deriving stock instance Generic Link

linkTable :: Table LinkWrite LinkRead
linkTable =
  table "links" $
    pLink
      Link
        { link_id = tableField "link_id"
        , link_uri = tableField "link_uri"
        , user_id = tableField "user_id"
        , created_at = tableField "created_at"
        }
