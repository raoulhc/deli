{-# LANGUAGE TemplateHaskell #-}

module Deli.Model.LinkTags where

import Deli.Prelude

import Data.Profunctor.Product.TH (makeAdaptorAndInstance)
import Opaleye

-- link tag boiler plate

type LinkTagPoly :: Type -> Type -> Type -> Type
data LinkTagPoly link_tag_id link_id tag_id = LinkTag
  { link_tag_id :: link_tag_id
  , link_id :: link_id
  , tag_id :: tag_id
  }

type LinkTagRead :: Type
type LinkTagRead = LinkTagPoly (Field SqlUuid) (Field SqlUuid) (Field SqlUuid)

type LinkTagWrite :: Type
type LinkTagWrite = LinkTagPoly (Maybe (Field SqlUuid)) (Field SqlUuid) (Field SqlUuid)

type LinkTag :: Type
type LinkTag = LinkTagPoly UUID UUID UUID

$(makeAdaptorAndInstance "pLinkTag" ''LinkTagPoly)

deriving stock instance Generic LinkTag

linkTagTable :: Table LinkTagWrite LinkTagRead
linkTagTable =
  table "link_tags" $
    pLinkTag
      LinkTag
        { link_tag_id = tableField "link_tag_id"
        , link_id = tableField "link_id"
        , tag_id = tableField "tag_id"
        }
