module Deli.Utils where

import Data.List.Extra (sortOn)
import Data.List.NonEmpty (NonEmpty (..), groupBy)

-- Taken from the definition in extrab but for nonempty lists
groupOn' :: Eq k => (a -> k) -> [a] -> [NonEmpty a]
groupOn' f = groupBy on2
 where
  on2 x = let fx = f x in \y -> fx == f y

groupSort' :: Ord k => [(k, v)] -> [(k, NonEmpty v)]
groupSort' = fmap (\((x, y) :| ys) -> (x, y :| fmap snd ys)) . groupOn' fst . sortOn fst
