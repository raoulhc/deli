module Deli.Routes where

import Deli.Prelude hiding ((:>))

import Servant

import Deli.Routes.Links

import Deli.Routes.Session
import Deli.Routes.Tags
import Deli.Routes.Users

type PageRoutes :: Type -> Type
data PageRoutes mode = PageRoutes
  { linksPage :: mode :- "links" :> Get '[HTML] (DeliResponse LinkPage)
  , usersPage :: mode :- "users" :> Get '[HTML] (DeliResponse UserPage)
  , tagsPage :: mode :- "tags" :> Get '[HTML] (DeliResponse TagPage)
  , loginPage :: mode :- "login" :> Get '[HTML] (DeliResponse LoginPage)
  , registerPage :: mode :- "register" :> Get '[HTML] (DeliResponse RegPage)
  }
  deriving stock (Generic)

type ApiRoutes :: Type -> Type
data ApiRoutes mode = ApiRoutes
  { linksApiRoutes :: mode :- "links" :> NamedRoutes LinkApiRoutes
  , usersApiRoutes :: mode :- "users" :> NamedRoutes UserApiRoutes
  , tagsApiRoutes :: mode :- "tags" :> NamedRoutes TagApiRoutes
  , sessionApiRoutes :: mode :- "session" :> NamedRoutes SessionApiRoutes
  }
  deriving stock (Generic)

type Routes :: Type -> Type
data Routes mode = Routes
  { home
      :: mode
        :- AuthProtect "optional-cookie-auth"
        :> Get '[JSON, HTML] (DeliResponse ())
  , pageRoutes :: mode :- AuthProtect "optional-cookie-auth" :> NamedRoutes PageRoutes
  , apiRoutes :: mode :- "api" :> NamedRoutes ApiRoutes
  , style :: mode :- "style.css" :> Raw
  , appjs :: mode :- "app.js" :> Raw
  }
  deriving stock (Generic)

type DeliAPI :: Type
type DeliAPI = ToServantApi Routes

api :: Proxy DeliAPI
api = genericApi (Proxy @Routes)
