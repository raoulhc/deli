{-# OPTIONS_GHC -Wno-orphans #-}

module Deli.Pages.Tags where

import Deli.Prelude

import Lucid.Base

import Deli.Routes.Links ()
import Deli.Routes.Tags

import Deli.Pages.Links ()

instance ToHtml TagPage where
  toHtmlRaw = toHtml
  toHtml _ =
    div_
      [ makeAttribute "hx-get" "/api/tags"
      , makeAttribute "hx-trigger" "load"
      , makeAttribute "hx-swap" "outerHTML"
      ]
      ""

instance ToJSON TagLinksResponse

instance ToHtml TagLinksResponse where
  toHtml TagLinksResponse{..} = do
    h1_ $ toHtml tag
    traverse_ toHtml links
  toHtmlRaw = toHtml

instance ToJSON TagsResponse

instance ToHtml TagsResponse where
  toHtml (TagsResponse tags) =
    traverse_
      (\tag -> a_ [href_ $ "tags/" <> tag] . p_ $ toHtml tag)
      tags
  toHtmlRaw = toHtml
