{-# OPTIONS_GHC -Wno-orphans #-}

module Deli.Pages.Users where

import Data.Time

import Deli.Prelude
import Lucid.Base

import Deli.Model.Users qualified as M
import Deli.Routes.Users

instance ToJSON M.User

-- TODO should go in an orphan folder
instance ToHtml UTCTime where
  toHtml = toHtml . show
  toHtmlRaw = toHtml

instance ToHtml M.User where
  toHtml (M.User{..}) = div_ [class_ "user", href_ $ "/links/" <> username] $ do
    a_ [class_ "user-user"] $ toHtml username
    a_ [class_ "user-created"] $ toHtml created_at
  toHtmlRaw = toHtml

instance ToHtml [M.User] where
  toHtml us = do
    title_ "Users"
    body_ $ case us of
      [] -> p_ "No users found!"
      _ -> traverse_ toHtml us
  toHtmlRaw = traverse_ toHtmlRaw

instance ToHtml UserPage where
  toHtml _ = do
    input_
      [ placeholder_ "Search"
      , type_ "text"
      , name_ "user"
      , makeAttribute "hx-get" "/api/users"
      , makeAttribute "hx-trigger" "keyup delay:500ms changed"
      , makeAttribute "hx-target" "#user-results"
      ]
    div_
      [id_ "user-results"]
      $ div_
        [ makeAttribute "hx-get" "/api/users"
        , makeAttribute "hx-trigger" "load once"
        , makeAttribute "hx-swap" "outerHTML"
        ]
        ""
  toHtmlRaw = toHtml
