{-# LANGUAGE QuasiQuotes #-}
{-# OPTIONS_GHC -Wno-orphans #-}

module Deli.Pages.Session where

import Deli.Prelude
import Lucid.Htmx
import Lucid.Hyperscript

import Lucid.Base

import Deli.Routes.Session

instance ToHtml LoginPage where
  toHtml _ = do
    div_ [class_ "content"] $ do
      h1_ "Login"
      form_ [hxPost_ "/api/session/login", makeAttribute "hx-target-error" "error"] $
        table_ [class_ "content"] $ do
          tr_ $ do
            td_ $ label_ [for_ "username"] "username"
            td_ $ input_ [id_ "username", name_ "username", type_ "text"]
          tr_ $ do
            td_ $ label_ [for_ "password"] "password"
            td_ $ input_ [id_ "password", name_ "password", type_ "password"]
          tr_ $ td_ [colspan_ "2"] $ input_ [type_ "submit", value_ "Login"]
          tr_ $ td_ [colspan_ "2"] $ div_ [class_ "error", id_ "error"] ""
  toHtmlRaw = toHtml

instance ToHtml RegPage where
  toHtml _ = do
    -- Hyperscript isn't quite powerful enough for string testing
    script_ "function validate_password(pw) { return /[A-Z]/.test(pw) && /[a-z]/.test(pw) && /[0-9]/.test(pw); }"
    div_ [class_ "content"] $ do
      h1_ "Register"
      form_
        [ hxPost_ "/api/session/register"
        , makeAttribute "hx-target-error" "error"
        ]
        $ table_ [class_ "content"]
        $ do
          tr_ $ do
            td_ $ label_ [for_ "username"] "Username"
            td_ $ input_ [id_ "username", name_ "username", type_ "text", required_ ""]
          tr_ $ do
            td_ $ label_ [for_ "email"] "E-mail"
            td_ $ input_ [id_ "email", name_ "email", type_ "email", required_ ""]
          tr_ $ do
            td_ $ label_ [for_ "password"] "password"
            td_ $
              input_
                [ id_ "password"
                , name_ "password"
                , type_ "password"
                , minlength_ "12"
                , [__|on keyup
                call validate_password(my.value) then set valid to it
                if not valid
                  call me.setCustomValidity("Password must contain uppercacse, lowercase and a number")
                else
                  call me.setCustomValidity("")
                end
                |]
                ]
          tr_ $ do
            td_ $ label_ [for_ "confim-password"] "confirm password"
            td_ $
              input_
                [ name_ "confirm-password"
                , id_ "confirm-password"
                , type_ "password"
                , minlength_ "12"
                , [__|on keyup
                  if #password.value != my.value
                    call me.setCustomValidity("Passwords must match!")
                  else
                    call me.setCustomValidity("")
                  end
                  |]
                ]
          tr_ $ td_ [colspan_ "2"] $ input_ [type_ "submit", value_ "Submit"]
          tr_ $ td_ [colspan_ "2"] $ div_ [class_ "error", id_ "error"] ""
  toHtmlRaw = toHtml

instance ToHtml LoginReject where
  toHtml _ = "User name or password invalid"
  toHtmlRaw = toHtml
