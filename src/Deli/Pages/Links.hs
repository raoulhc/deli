{-# OPTIONS_GHC -Wno-orphans #-}

module Deli.Pages.Links where

import Deli.Prelude hiding ((:>))

import Lucid.Base
import Lucid.Htmx

import Data.List (intersperse)
import Data.Vector qualified as V

import Deli.Routes.Links

instance ToHtml LinkResponse where
  toHtml (LinkResponse link user v_tags) =
    div_ [class_ "link"] $ do
      div_ $ do
        a_ [class_ "link-link", href_ $ "http://" <> link] . ins_ $ toHtml link
        " "
        a_ [class_ "link-user", href_ $ "/users/" <> user] $ toHtml user
      p_ [class_ "link-tags"] $ do
        "tags: "
        -- Add tag link later on
        sequence_ . intersperse ", " . V.toList $
          fmap
            ( \tag ->
                a_
                  [ class_ "link-tag"
                  , href_ $ "/tags/" <> tag
                  ]
                  $ toHtml
                  $ "#" <> tag
            )
            v_tags
  toHtmlRaw = toHtml

instance ToHtml [LinkResponse] where
  toHtml = traverse_ toHtml
  toHtmlRaw = traverse_ toHtmlRaw

instance ToHtml LinkPage where
  toHtmlRaw = toHtml
  toHtml (LinkPage loggedIn) = do
    -- If we're logged in give us a form for posting links
    when loggedIn
      . div_ [class_ "post-link"]
      . form_
        [ hxPost_ "/api/links"
        , autocomplete_ "off"
        ]
      $ do
        p_ $ do
          label_ [for_ "link"] "Link"
          input_
            [ type_ "text"
            , id_ "link"
            , name_ "link_uri"
            ]
        p_ $ do
          label_ [for_ "tags"] "Tags"
          input_
            [ type_ "text"
            , id_ "tags"
            , name_ "tags"
            ]
        p_ $
          input_
            [type_ "submit", value_ "Post link", onclick_ "/links"]
    -- Load data
    div_
      [ makeAttribute "hx-get" "/api/links"
      , makeAttribute "hx-trigger" "load"
      , makeAttribute "hx-swap" "outerHTML"
      ]
      ""
