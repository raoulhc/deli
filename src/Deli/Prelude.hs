{-# OPTIONS_GHC -Wno-orphans #-}

module Deli.Prelude
  ( module Data.Proxy
  , module Control.Arrow
  , module Control.Monad
  , module Data.Maybe
  , module Data.Kind
  , module Data.Function
  , module Data.Traversable
  , module Data.Foldable
  , module GHC.Generics
  -- Core libraries
  , module Data.ByteString
  , module Data.Text
  , module Data.UUID
  , module Data.Vector
  , module Data.Aeson
  , module Effectful
  , module Effectful.Reader.Static
  , module Effectful.Error.Static
  , module Effectful.Log
  , module Data.Text.Display
  , module Lucid
  , module Servant.HTML.Lucid
  -- Internal
  , module Deli.Types
  , module Effectful.Opaleye
  ) where

-- Base and GHC

import Control.Arrow (Arrow (..), (>>>))
import Control.Monad (void, when)
import Data.Foldable (traverse_)
import Data.Function ((&))
import Data.Kind (Type)
import Data.Maybe (fromMaybe, isJust)
import Data.Proxy (Proxy (..))
import Data.Traversable (for)
import GHC.Generics

-- Core libraries
import Data.ByteString (ByteString)
import Data.Text (Text)
import Data.UUID (UUID)
import Data.Vector (Vector)

import Effectful (Eff, IOE, liftIO, (:>))
import Effectful.Error.Static (Error)
import Effectful.Log
  ( Log
  , Logger
  , logAttention
  , logAttention_
  , logInfo
  , logInfo_
  , logTrace
  , logTrace_
  )
import Effectful.Reader.Static (Reader, ask, asks)

-- Lucid
import Lucid
import Servant.HTML.Lucid (HTML)

import Data.Aeson (FromJSON (..), ToJSON (..), decode, encode)

import Data.Text.Display (Display (..), display)

-- Internal modules
import Deli.Types
import Effectful.Opaleye

instance ToHtml () where
  toHtml _ = pure ()
  toHtmlRaw _ = pure ()
