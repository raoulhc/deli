module Deli.Types where

import Data.Aeson
import Data.Kind
import Data.Text (Text)
import Data.Text.Display
import GHC.Generics

import Effectful
import Effectful.Error.Static
import Effectful.Log
import Effectful.Opaleye
import Effectful.Reader.Static

import Servant
import Servant.Auth.Server

type DeliCtx :: Type
data DeliCtx = DeliCtx
  { cookieSettings :: CookieSettings
  , jwtSettings :: JWTSettings
  }

type Deli :: Type -> Type
type Deli = Eff '[Reader DeliCtx, Error ServerError, Log, DB, IOE]

-- Authenticated user type
type User :: Type
newtype User = User Text
  deriving stock (Generic)
  deriving newtype (Display)

instance ToJSON User
instance FromJSON User
instance ToJWT User
instance FromJWT User

-- This is largely to give us a way to capture context for the HTML
-- representation while still responding the same thing for JSON
type DeliResponse :: Type -> Type
data DeliResponse a = DeliResponse {mUser :: Maybe User, deliData :: a}
