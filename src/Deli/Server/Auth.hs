module Deli.Server.Auth where

import Deli.Prelude

import Database.PostgreSQL.Simple (Connection)
import Effectful.Error.Static qualified as Eff
import Network.HTTP.Types (hLocation)

import Network.Wai (Request, requestHeaders)
import Servant hiding ((:>))
import Servant.Auth.Server (clearSession, verifyJWT)
import Servant.Server.Experimental.Auth

import Crypto.JOSE.JWK (JWK)
import Web.Cookie

import Deli.Server.Utils (effToHandler)

type instance AuthServerData (AuthProtect "cookie-auth") = User

type instance AuthServerData (AuthProtect "optional-cookie-auth") = Maybe User

-- Get a user from cookies
checkCookie
  :: ( Reader DeliCtx :> es
     , Error ServerError :> es
     , Log :> es
     , IOE :> es
     )
  => ByteString
  -> Eff es User
checkCookie cookie = do
  logInfo_ "Checking cookie"
  jwt <- asks jwtSettings
  res <- liftIO $ verifyJWT jwt cookie
  case res of
    Just user -> do
      logTrace_ "Validated user"
      pure user
    Nothing -> do
      logInfo_ "Couldn't validate user. Clearing cookies."
      cookieSettings <- asks cookieSettings
      let clearHeaders = getHeaders $ clearSession cookieSettings NoContent
          location = (hLocation, "/" :: ByteString)
      Eff.throwError err303{errHeaders = location : clearHeaders}

-- For when we need authentication to access an end point
authHandler :: JWK -> Connection -> Logger -> AuthHandler Request User
authHandler jwk conn logger =
  mkAuthHandler
    (effToHandler jwk conn logger . handler)
 where
  -- Largerly grabbed from servant docs
  throw401 msg = Eff.throwError $ err401{errBody = msg}
  maybeToEither msg = maybe (Left msg) Right
  handler req = either throw401 checkCookie $ do
    cookie <-
      maybeToEither "Couldn't find cookie" $
        lookup "cookie" $
          requestHeaders req
    maybeToEither "Couldn't find auth cookie" $
      lookup "JWT-Cookie" $
        parseCookies cookie

-- For optional authentication end points
optAuthHandler
  :: JWK
  -> Connection
  -> Logger
  -> AuthHandler Request (Maybe User)
optAuthHandler jwk conn logger =
  mkAuthHandler
    (effToHandler jwk conn logger . handler)
 where
  handler req =
    traverse checkCookie $
      lookup "cookie" (requestHeaders req)
        >>= (lookup "JWT-Cookie" . parseCookies)
