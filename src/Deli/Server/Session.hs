{-# LANGUAGE MultiWayIf #-}

module Deli.Server.Session where

import Deli.Prelude

import Data.ByteString.Base64
import Data.Maybe (listToMaybe)
import Data.Text.Encoding
import Sel.Hashing.Password
import Servant
  ( Headers
  , NoContent (..)
  , Union
  , WithStatus (..)
  , respond
  )
import Servant.Auth.Server (acceptLogin, clearSession)
import Servant.Server.Generic (AsServerT)

import Opaleye
  ( Insert (Insert)
  , rCount
  , selectTable
  , sqlStrictText
  , where_
  , (.==)
  )

import Deli.Model.Users qualified as M
import Deli.Routes.Session
import Deli.Server.Utils

loginHandler
  :: (IOE :> es, Reader DeliCtx :> es, DB :> es, Log :> es)
  => LoginForm
  -> Eff es (Union LoginResponses)
loginHandler l = do
  logInfo_ "Handling login attempt"
  mUser <- fmap listToMaybe . select @_ @_ @M.User $ do
    u <- selectTable M.userTable
    where_ $ u.username .== sqlStrictText (l.username)
    pure u
  case mUser of
    Nothing -> do
      logInfo_ $ "Rejected login attempt for " <> l.username <> ": doesn't exist"
      respond . WithStatus @401 $ LoginReject
    Just u -> do
      -- verify user
      let salt =
            fromMaybe (error "Corrupt salt in db!") $
              binaryToSalt $
                decodeBase64Lenient $
                  encodeUtf8 u.password_salt
      let passwordHash = decodeBase64Lenient $ encodeUtf8 u.password_hash
      loginHash <-
        liftIO $
          passwordHashToBinary
            <$> hashByteStringWithParams
              defaultArgon2Params
              salt
              (encodeUtf8 l.password)
      if loginHash /= passwordHash
        then do
          logInfo_ $ "Rejected login attempt for " <> l.username <> ": wrong password"
          respond $ WithStatus @401 LoginReject
        else do
          let user = User u.username
          DeliCtx{..} <- ask
          mApplyCookie <- liftIO $ acceptLogin cookieSettings jwtSettings user
          case mApplyCookie of
            Just applyCookie -> do
              logInfo_ $ "Logged in for " <> l.username
              respond
                . WithStatus @303
                . applyCookie
                . addHxRedirect "/links"
                $ NoContent
            Nothing -> respond . WithStatus @401 $ LoginReject

logoutHandler
  :: Reader DeliCtx :> es
  => Eff
      es
      (Headers LogoutHeaders NoContent)
logoutHandler = do
  cookieSettings <- asks cookieSettings
  pure
    . clearSession cookieSettings
    . addHxRedirect "/links"
    $ NoContent

registerHandler
  :: Log :> es
  => DB :> es
  => IOE :> es
  => RegisterForm
  -> Eff
      es
      ( Union
          '[ WithStatus 200 RegistrationAccepted
           , WithStatus 400 RegistrationRejected
           ]
      )
registerHandler r = do
  logInfo_ "Handling registration attempt"
  logTrace_ $ display r
  userClashes :: [M.User] <- select $ do
    u <- selectTable M.userTable
    where_ $ u.username .== sqlStrictText r.username
    pure u
  emailClashes :: [M.User] <- select $ do
    u <- selectTable M.userTable
    where_ $ u.email .== sqlStrictText r.email
    pure u
  if not $ null userClashes && null emailClashes
    then do
      logInfo_ "Rejecting registration attempt"
      respond $
        WithStatus @400 $
          RegistrationRejected $
            if
                | null userClashes -> "Email already registered"
                | null emailClashes -> "User exists regestered"
                | otherwise -> "User and email already registered"
    else do
      logTrace_ "Calculating hashes"
      salt <- liftIO genSalt
      hash <- liftIO $ hashByteStringWithParams defaultArgon2Params salt (encodeUtf8 r.password)
      let saltBs = saltToBinary salt
          newUser =
            M.User
              Nothing
              (sqlStrictText r.username)
              (sqlStrictText r.email)
              (sqlStrictText $ encodeBase64 $ passwordHashToBinary hash)
              (sqlStrictText $ encodeBase64 saltBs)
              Nothing
              Nothing
      void $ insert $ Insert M.userTable [newUser] rCount Nothing
      respond $ WithStatus @200 RegistrationAccepted

sessionApiHandler :: SessionApiRoutes (AsServerT Deli)
sessionApiHandler =
  SessionApiRoutes
    { sessionLogin = loginHandler
    , sessionLogout = logoutHandler
    , sessionRegister = registerHandler
    }
