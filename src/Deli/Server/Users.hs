module Deli.Server.Users where

import Deli.Model.Users qualified as M
import Deli.Prelude
import Deli.Routes.Users
import Servant.Server.Generic

getUsersHandler :: DB :> es => Maybe Text -> Eff es [M.User]
getUsersHandler =
  maybe M.getUsers (fmap (maybe [] pure) . M.getUser)

usersApiHandler :: UserApiRoutes (AsServerT Deli)
usersApiHandler =
  UserApiRoutes
    { getUsers = getUsersHandler
    }
