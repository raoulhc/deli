module Deli.Server.Links where

import Deli.Prelude hiding (for_)
import Deli.Utils (groupSort')

import Data.Foldable (for_)

import Data.List.NonEmpty (NonEmpty (..))
import Data.Vector qualified as V
import Opaleye

import Servant hiding ((:>))
import Servant.Server.Generic

import Deli.Model.LinkTags qualified as M
import Deli.Model.Links qualified as M
import Deli.Model.Tags qualified as M
import Deli.Model.Users qualified as M

import Deli.Server.Auth ()

import Deli.Routes.Links
import Deli.Server.Utils

getLinksQuery
  :: DB :> es
  => (M.LinkRead -> M.LinkTagRead -> M.TagRead -> M.UserRead -> Select ())
  -> Eff es [LinkResponse]
getLinksQuery constr = do
  -- No idea if this is a good solution but we grab ((link_id, uri), (uri, tag)) so we
  -- can group by id's and then easily have uri to a list of tags
  -- Because we get a big list with lots of duplicates
  ls :: [((UUID, Text), (Text, Text))] <- select $ do
    l <- selectTable M.linkTable
    lt <- selectTable M.linkTagTable
    where_ $ l.link_id .== lt.link_id
    t <- selectTable M.tagTable
    where_ $ lt.tag_id .== t.tag_id
    u <- selectTable M.userTable
    where_ $ l.user_id .== u.user_id
    constr l lt t u
    pure ((l.link_id, u.username), (l.link_uri, t.tag))
  pure $
    ( \((_, user), (uri, tag) :| uri_tags) ->
        LinkResponse uri user $ V.fromList $ tag : fmap snd uri_tags
    )
      <$> groupSort' ls

getLinksHandler :: DB :> es => Eff es [LinkResponse]
getLinksHandler =
  getLinksQuery (\_ _ _ _ -> where_ (sqlBool True))

getUserLinksHandler
  :: DB :> es => Text -> Eff es [LinkResponse]
getUserLinksHandler user =
  getLinksQuery
    ( \_ _ _ M.User{..} ->
        where_ (sqlStrictText user .== username)
    )

-- TODO Change to Link include user later, should be from authenticated users
-- TODO also add ore potential response types, people could post invalid links
postLinksHandler
  :: (Log :> es, DB :> es)
  => User
  -> LinkResponse
  -> Eff es (Headers '[Header "HX-Redirect" Text] NoContent)
postLinksHandler (User _n) l@LinkResponse{..} = do
  logInfo "post request" l
  user_id :: UUID <- fmap head . select $ do
    u <- selectTable M.userTable
    where_ $ "raoul" .== u.username
    pure u.user_id
  M.Link l_id _ _ _ :: M.Link <-
    fmap head . insert $
      Insert
        M.linkTable
        [M.Link Nothing (sqlStrictText link_uri) (sqlUUID user_id) Nothing]
        (rReturning id)
        Nothing
  let tag_rows = M.Tag Nothing . sqlStrictText <$> tags
  void
    . insert
    $ Insert
      M.tagTable
      (V.toList tag_rows)
      rCount
      (Just doNothing)
  tag_ids :: [UUID] <- select $ do
    M.Tag t_id tag <- selectTable M.tagTable
    where_ $ in_ (sqlStrictText <$> tags) tag
    pure t_id
  for_ tag_ids $ \tag_id ->
    insert $
      Insert
        M.linkTagTable
        [M.LinkTag Nothing (sqlUUID l_id) (sqlUUID tag_id)]
        rCount
        Nothing
  pure $ addHxRedirect "/links" NoContent

linksApiHandler :: LinkApiRoutes (AsServerT Deli)
linksApiHandler =
  LinkApiRoutes
    { getLinks = getLinksHandler
    , getUserLinks = getUserLinksHandler
    , postLink = postLinksHandler
    }
