module Deli.Server.Tags where

import Deli.Prelude

import Data.Maybe (listToMaybe)
import Opaleye

import Deli.Model.Tags qualified as M
import Servant.Server.Generic

import Deli.Routes.Tags
import Deli.Server.Links

tagsApiHandler :: TagApiRoutes (AsServerT Deli)
tagsApiHandler =
  TagApiRoutes
    { getTags = getTagsHandler
    , getTag = getTagHandler
    }

getTagsHandler :: DB :> es => Eff es TagsResponse
getTagsHandler =
  TagsResponse . fmap M.tag
    <$> select @_ @_ @M.Tag (selectTable M.tagTable)

getTagHandler :: DB :> es => Text -> Eff es TagLinksResponse
getTagHandler tag = do
  tag_id :: Maybe UUID <- fmap listToMaybe . select $ do
    M.Tag t_id t_tag <- selectTable M.tagTable
    where_ $ sqlStrictText tag .== t_tag
    pure t_id
  case tag_id of
    Just tag_id' -> do
      links <-
        getLinksQuery
          ( \_ _ (M.Tag t_id _) _ ->
              where_ (t_id .== sqlUUID tag_id')
          )
      pure $ TagLinksResponse tag links
    Nothing -> pure $ TagLinksResponse tag []
