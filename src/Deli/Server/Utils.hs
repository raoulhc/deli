module Deli.Server.Utils where

import Deli.Prelude

import Effectful (runEff)
import Effectful.Error.Static (runErrorNoCallStack)
import Effectful.Log (LogLevel (..), runLog)
import Effectful.Reader.Static (runReader)

import Crypto.JOSE.JWK (JWK)
import Database.PostgreSQL.Simple (Connection)

import Servant
import Servant.Auth.Server

-- This only works when adding the first header
addHxRedirect :: Text -> xs -> Headers '[Header "HX-Redirect" Text] xs
addHxRedirect = addHeader

addHxRefresh :: xs -> Headers '[Header "HX-Refresh" Bool] xs
addHxRefresh = addHeader True

effToHandler :: JWK -> Connection -> Logger -> Deli a -> Handler a
effToHandler jwk connection logger action = do
  res <-
    action
      & runReader (DeliCtx defaultCookieSettings $ defaultJWTSettings jwk)
      & runErrorNoCallStack @ServerError
      & runLog "Deli" logger LogTrace
      & runOpaleye connection
      & runEff
      & liftIO
  either throwError pure res
