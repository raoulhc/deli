module Deli.Style where

import Clay
import Prelude hiding (id)

import Data.Text.Lazy.Encoding (encodeUtf8)
import Network.HTTP.Types
import Network.Wai
import Servant

-- WAI interface to just respond with a bytestring of our clay described css
styleHandler :: ServerT Raw m
styleHandler = Tagged $ \_ resp ->
  resp . responseLBS status200 [] . encodeUtf8 . render $ do
    body ? do
      overflow hidden
      background nord1
      fontColor nord5
      margin (px 0) 0 0 0
    header ? do
      overflow hidden
      background nord0
      fontColor nord5
      fontSizeCustom xxLarge
      padding (px 5) 20 5 20
    ".content-box" ? do
      padding (px 10) 10 10 10
      margin auto auto auto auto
    ".content" ? do
      margin auto auto auto auto
      textAlign center
    navStyling
    userStyling
    linkStyling
    formStyling
    ".error" ? do
      fontColor nord11
      textAlign center

navStyling :: Css
navStyling = do
  ".topnav" ? do
    overflow hidden
    background nord2

  ".topnav" |> a ? do
    float floatLeft
    navAnchorStyle

  ".topnav-right" |> a ? do
    float floatRight
    navAnchorStyle
 where
  navAnchorStyle = do
    textAlign center
    display inline
    padding (px 10) 10 10 10
    color nord4
    textDecoration none
    hover & background nord3
    active & color nord13

formStyling :: Css
formStyling = do
  label ? do
    color nord4
  input ? do
    background nord4
    color nord1
    padding (px 5) 5 5 5
    borderColor transparent
    borderWidth4 (px 0) 5 0 0
    boxShadow $ pure none
  input # selected ? do
    borderColor nord8
  input # ("type" @= "submit") ? do
    width (pct 100)
  input # invalid ? do
    borderStyle solid
    borderColor nord11

userStyling :: Css
userStyling = do
  ".user" ? do
    padding (px 10) 10 10 10
    color nord4
    background nord2
    hover & background nord3

  ".user-user" ? do
    padding (px 10) 10 10 10

linkStyling :: Css
linkStyling = do
  ".link" ? do
    padding (px 10) 10 10 10
    textDecoration none
    background nord3
    borderStyle solid
    borderColor nord0
    overflow hidden
    hover & background nord2

  ".link-link" ? do
    textDecoration none
    fontSizeCustom xLarge
    color nord12

  ".link-tag" ? do
    textDecoration none
    color nord11

-- Nord colors

-- Polar night
nord0, nord1, nord2, nord3 :: Color
nord0 = "#2e3440"
nord1 = "#3b4252"
nord2 = "#434c5e"
nord3 = "#4c566a"

-- Snow storm
nord4, nord5, nord6 :: Color
nord4 = "#d8dee9"
nord5 = "#e5e9f0"
nord6 = "#eceff4"

-- Frost
nord7, nord8, nord9, nord10 :: Color
nord7 = "#8fbcbb"
nord8 = "#88c0d0"
nord9 = "#81a1c1"
nord10 = "#5e81ac"

-- Aurora
nord11, nord12, nord13, nord14, nord15 :: Color
nord11 = "#bf616a"
nord12 = "#d08770"
nord13 = "#ebcb8b"
nord14 = "#a3be8c"
nord15 = "#b48ead"
