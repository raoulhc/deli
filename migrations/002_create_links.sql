create table if not exists tags (
  tag_id uuid primary key default gen_random_uuid(),
  tag text not null unique
);


create table if not exists links (
  link_id uuid primary key default gen_random_uuid(),
  link_uri text not null,
  user_id uuid not null,
  foreign key (user_id) references users(user_id),
  created_at timestamptz default now()
);

create table if not exists link_tags (
  link_tag_id uuid primary key default gen_random_uuid(),
  link_id uuid not null,
  foreign key (link_id) references links(link_id),
  tag_id uuid not null,
  foreign key (tag_id) references tags(tag_id)
);
