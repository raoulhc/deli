create table if not exists users (
  user_id uuid primary key default gen_random_uuid(),
  username text unique not null,
  email text unique not null,
  password_hash text not null,
  password_salt text not null,
  created_at timestamptz default now(),
  updated_at timestamptz default now()
);

create unique index on users (lower(username));
create unique index on users (lower(email));
