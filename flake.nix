{
  description = "horizon-minimal-template";

  inputs = {
    flake-utils.url = "github:numtide/flake-utils";

    horizon-platform.url = "git+https://gitlab.homotopic.tech/horizon/horizon-platform";

    nixpkgs.url = "github:nixos/nixpkgs/nixpkgs-unstable";

    hs-opaleye.url = "github:tomjaguarpaw/haskell-opaleye";
    hs-opaleye.flake = false;
    hs-dotenv.url = "github:stackbuilders/dotenv-hs";
    hs-dotenv.flake = false;
    hs-product-profunctors.url = "github:tomjaguarpaw/product-profunctors";
    hs-product-profunctors.flake = false;
    hs-clay.url = "github:sebastiaanvisser/clay";
    hs-clay.flake = false;
    hs-lucid-htmx.url = "github:monadicsystems/lucid-htmx";
    hs-lucid-htmx.flake = false;
    hs-lucid-hyperscript.url = "github:monadicsystems/lucid-hyperscript";
    hs-lucid-hyperscript.flake = false;
    hs-libsodium-bindings.url = "/home/rhidalgochar/haskell/libsodium-bindings";
    # hs-libsodium-bindings.url = "github:haskell-cryptography/libsodium-bindings";
    hs-libsodium-bindings.flake = false;
    hs-base16.url = "github:emilypi/Base16/fb313f343e145b57687cc98e6ad49c88fc5fa485";
    hs-base16.flake = false;
  };

  outputs =
    inputs@
    { self
    , flake-utils
    , horizon-platform
    , nixpkgs
    , hs-opaleye
    , hs-dotenv
    , hs-product-profunctors
    , hs-clay
    , hs-lucid-htmx
    , hs-lucid-hyperscript
    , hs-libsodium-bindings
    , hs-base16
    , ...
    }:
    flake-utils.lib.eachSystem [ "x86_64-linux" ] (system:
    let
      pkgs = import nixpkgs { inherit system; };
    in
    with pkgs.haskell.lib;
    let

      myOverlay = final: prev: {
        opaleye = dontCheck (final.callCabal2nix "opaleye" "${hs-opaleye}/" { });
        dotenv = dontCheck (doJailbreak (final.callCabal2nix "dotenv" "${hs-dotenv}/" { }));
        product-profunctors = final.callCabal2nix "product-profunctors" "${hs-product-profunctors}/" { };
        clay = final.callCabal2nix "clay" "${hs-clay}/" { };
        lucid-htmx = doJailbreak (final.callCabal2nix "lucid-htmx" "${hs-lucid-htmx}/lucid1" { });
        lucid-hyperscript = doJailbreak (final.callCabal2nix "lucid-hyperscript" "${hs-lucid-hyperscript}/" { });

        libsodium-bindings =
          addBuildDepends
          (enableCabalFlag
            (final.callCabal2nix "libsodium-bindings"
              "${hs-libsodium-bindings}/libsodium-bindings"
              { })
              "use-pkg-config")
              [ pkgs.pkg-config pkgs.libsodium ];
        sel = final.callCabal2nix "sel" "${hs-libsodium-bindings}/sel" { };

        base16 = final.callCabal2nix "base16" "${hs-base16}/" { };
        fourmolu =
          addBuildDepend (disableCabalFlag prev.fourmolu "fixity-th")
          prev.file-embed;

        deli = prev.callCabal2nix "deli" ./. { };
      };

      legacyPackages = horizon-platform.legacyPackages.${system}.extend myOverlay;

    in
    {

      devShells.default = legacyPackages.deli.env.overrideAttrs (attrs: {
        buildInputs = attrs.buildInputs ++ (with pkgs; with legacyPackages; [
          postgresql
          postgresql-migration
          cabal-install
          haskell-language-server
          ghcid
          nginx
          fourmolu
          pkg-config
          libsodium
          libsodium.out
        ]);
      });

      packages.default = legacyPackages.deli;
    });
}
