# Deli

A link sharing website that you can deploy within private networks.

Inspired by Delicious, with the idea conjured in NYC so the name Deli seemed
apt.

Largely for me to personally to get to grips with servant and web apps, but
maybe it'll end up useful for someone else.

Based around servant for the framework, with htmx being used for a fairly
simple frontend.

## Development

Use nix

```
nix develop
```
